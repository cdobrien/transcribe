#!/usr/bin/python3

import os
import sys
import tempfile
import speech_recognition as sr
import shutil
from pydub import AudioSegment

WORK_DIR = "tmp"

def convert_to_wav(mp3_file):
    wav_filename = mp3_file.replace(".mp3", ".wav")
    audio = AudioSegment.from_mp3(mp3_file)
    audio.export(wav_filename, format="wav")
    return wav_filename

def get_audio_data_length(audio_data):
    name = None
    with tempfile.NamedTemporaryFile(delete=False) as f:
        name = f.name
        f.write(audio_data.get_wav_data())
    duration = None
    with sr.AudioFile(name) as audio_file:
        duration = audio_file.DURATION
    os.remove(name)
    return duration

def get_phrase(sr, filename, index):
    remaining = None
    phrase = None
    with sr.AudioFile(filename) as source:
        if source.DURATION < 0.1:
            return False
        phrase = r.listen(source)
        remaining = r.record(source)
    phrase_filename = os.path.join(WORK_DIR, str(index) + ".wav")
    with open(phrase_filename, "wb") as f:
        f.write(phrase.get_wav_data())
    with open(filename, "wb") as f:
        f.write(remaining.get_wav_data())
    return True

def split_into_phrases(sr, filename):
    index = 0
    while get_phrase(sr, filename, index):
         index += 1

def split_file(in_file):
    r.pause_threshold = 0.5
    tmp_file = None
    with tempfile.NamedTemporaryFile(delete=False) as f:
        tmp_file = f.name
    shutil.copyfile(in_file, tmp_file)
    split_into_phrases(sr, tmp_file)
    os.remove(tmp_file)

def transcribe_phrase(in_file, out_file):
    with sr.AudioFile(in_file) as source:
        data = r.record(source)
    try:
        transcript = r.recognize_google(data)
        with open(out_file, "a") as f:
            f.write("%s\n" % transcript)
        os.remove(in_file)
    except:
        print("%s failed" % in_file)
        with open(out_file, "a") as f:
            f.writeline("---- missing section (%s) ----\n" % in_file)
        path = in_file.replace("%s/" % WORK_DIR, "")
        shutil.move(in_file, path)

def transcribe(out_file):
    for file in os.listdir(WORK_DIR):
        if file.endswith(".wav"):
            path = "%s/%s" % (WORK_DIR, file)
            transcribe_phrase(path, out_file)
        else:
            continue

if __name__ == "__main__":
    in_file = sys.argv[1]
    out_file = sys.argv[2]
    rm_in_file = False
    if in_file.endswith(".mp3"):
        in_file = convert_to_wav(in_file)
        rm_in_file = True

    os.mkdir(WORK_DIR)
    r = sr.Recognizer()
    split_file(in_file)
    transcribe(out_file)
    os.rmdir(WORK_DIR)
    if rm_in_file:
        os.remove(in_file)
