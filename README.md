# Transcribe

A script for *noice* audio transcription.

## Usage

```
$ ./transcribe.sh <audio_file> <transcript_file>
```

MP3s, FLACs and WAVs should all work.

## Dependencies / Set-Up

- Python 3
- `SpeechRecognition`
- `pydub`
- `ffmpeg` (required for `pydub`)

### Linux

Install the `python3`, `python3-pip` and `ffmpeg` packages (or whatever your
distro calls them), then use `pip` to install `pydub` and `SpeechRecognition`.

On Ubuntu:

```
$ sudo apt-get install python3 python3-pip ffmpeg
$ sudo pip3 install pydub SpeechRecognition
```

### Windows

You can download a windows installer for Python 3 at
[python.org](https://www.python.org/downloads/). Once that's installed run
```
> pip install pydub SpeechRecognition
```
in command prompt or powershell to install the required python libraries.
